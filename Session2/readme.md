### Here is a table showing approximate  duration of running sum with pybind11 using np-array and vector :



 ## numpy array version 
 * 4 threads used in parallel version
 * code file is [here](Session2/pybind-codes/wrap.cpp)
 * commands to run these files are located in the readme file of  [this](Session2/pybind-codes) directory


| Input Size    | pybind-simple-sum  | pybind-omp-sum |
| ------------- |:------------------:|:-------------: |
| 10            | 0.0000758 sec      | 0.0016741 sec  | 
| 1k            | 0.0000460 sec      | 0.0013022 sec  | 
| 100k          | 0.0003161 sec      | 0.0009469 sec  | 
| 1M            | 0.0028481 sec      | 0.0034468 sec  | 



## vector version 
* 4 threads used in parallel version
* code file is [here](Session2/pybind-codes/wrap2.cpp)
* commands to run these files are located in the readme file of [this](Session2/pybind-codes) directory

| Input Size    | pybind-simple-sum  | pybind-omp-sum |
| ------------- |:------------------:|:-------------: |
| 10            | 0.0000078 sec      | 0.0008206 sec  | 
| 1k            | 0.0002748 sec      | 0.0025708 sec  | 
| 100k          | 0.0093185 sec      | 0.0100674 sec  | 
| 1M            | 0.0882394 sec      | 0.0807893 sec  | 


## Results plot

### pybind-plot
![numpy and vector](Session2/resultplot.png)

### general-plot

![general comparison](Session2/general.png)

### A more precise plot 

![precise plot](Session2/precise.png)
