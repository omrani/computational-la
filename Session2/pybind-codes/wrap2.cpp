<%
cfg['compiler_args'] = ['-std=c++11', '-fopenmp']
cfg['linker_args'] = ['-fopenmp']
setup_pybind11(cfg)
%>

#include <omp.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>

using namespace std;
namespace py = pybind11;

long int simple(std::vector <long int> &vc) {
  long int sum=0;
  // double start = omp_get_wtime();
	for(int i=0;i<(long int)vc.size();i++)
		sum+=vc[i];
    // double end = omp_get_wtime();
    return sum;
}

long int parallel(std::vector<long int> &vc){
    long int sum = 0;
    int max= omp_get_max_threads();
    omp_set_num_threads(max);

      #pragma omp parallel for reduction(+:sum)
    	for(int i=0;i<(long int)vc.size();i++){
        sum += vc[i];
    }
    return sum;
}


PYBIND11_MODULE(wrap2,m){
m.doc() = "auto-compiled c++ extension";
m.def("simple",&simple);
m.def("parallel", &parallel);
}
