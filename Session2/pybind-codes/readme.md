## Here I have applied the cppimport method in working with pybind11 



1.  Be sure to have pybind11, cppimport and openmp library installed first:

>   `$ pip install pybind11`  
>
>   `$ pip install cppimport`  
> 
>   `$ sudo apt-get install libomp-dev` 



2.  Run `test_file.py`  and insert the size of your input array 
 
>  ` $ python test_file.py `


3. This will create an `.so` file in your directory that is the library you can import in any python file you want to use the functions in `wrap.cpp` and `wrap2.cpp`


4. You will get run times of parallel sum and simple sum versions as output
