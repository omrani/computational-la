<%
cfg['compiler_args'] = ['-std=c++11', '-fopenmp']
cfg['linker_args'] = ['-lgomp']
setup_pybind11(cfg)
%>

#include <omp.h>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#define MAX_THREADS 8

namespace py = pybind11;


long int simple(py::array_t<int> xs) {

    py::buffer_info info = xs.request();
    int * ptr1 = (int *)info.ptr;

    int n=info.shape[0];
    long  int sum=0;
    // double start = omp_get_wtime();
    for (int i = 0; i <n; i++) {
        sum+=ptr1[i];
    }
    // double end = omp_get_wtime();
    return sum;
}


long int parallel(py::array_t<int> xs) {

    py::buffer_info info = xs.request();
    int * ptr = (int *)info.ptr;
    int max= omp_get_max_threads();
    omp_set_num_threads(max);
    int n=info.shape[0];
    long int sum=0;
    #pragma omp parallel for reduction (+:sum)
    for (int i = 0; i < n; i++) {
        sum+=ptr[i];
    }
    return sum;
}


PYBIND11_MODULE(wrap, m) {
  m.doc() = "auto-compiled c++ extension";
  m.def("simple",&simple);
  m.def("parallel", &parallel);
}
