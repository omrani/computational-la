import cppimport
import time
import numpy as np
import random

code = cppimport.imp("wrap")
# code = cppimport.imp("wrap2")

n= int(input())
xs = np.random.randint(1,n,n)
xs2=random.sample(range(n), n)




start= time.time()
code.simple(xs)
end = time.time()
print(end-start," simple-pybind");



start= time.time()
#print(code.psum(xs))
code.parallel(xs)
end = time.time()
print(end-start," parallel-pybind");
