## Results taken from Numpy gradient descent on Concrete_Data.csv and height_wieght_genders.csv :
```
 Method = Numpy 
 max iterations = 50 

```

 ### height_wieght_genders

| gamma        | Time          | objective    |
| ------------:|:-------------:|:------------:|
| 0.00001      | 0.023 seconds | 15.3858887   | 
| 0.0001       | 0.030 seconds | 2792.23671   | 
| 0.001        | 0.032 seconds | 5.77305e+128 | 



 ### Concrete_Data

| gamma        | Time          | objective       |
| ------------:|:-------------:|:---------------:|
| 0.00001      | 0.017 seconds | 709.018885537   | 
| 0.0001       | 0.015 seconds | 696.323306598   | 
| 0.001        | 0.018 seconds | 1.42130668e+55  |
   



