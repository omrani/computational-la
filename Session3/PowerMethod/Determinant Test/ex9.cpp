<%
cfg['compiler_args'] = ['-std=c++11']
cfg['include_dirs'] = ['./eigen']
setup_pybind11(cfg)
%>

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include<iostream>

#include <Eigen/LU>
namespace py = pybind11;



// takes numpy array as input and returns double
void det(Eigen::MatrixXd xs) {

     xs.determinant();
}


PYBIND11_MODULE(ex9, m) {
    m.doc() = "auto-compiled c++ extension";
    m.def("det", &det);
}
