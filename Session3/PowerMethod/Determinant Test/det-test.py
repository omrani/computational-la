import cppimport
import time
import numpy as np
import scipy.linalg as la


code = cppimport.imp("ex9")

n=int(input())
A = np.random.rand(n,n)


start=time.time()
code.det(A)
end=(time.time())
print("eigen :",end-start)


# start=time.time()
# la.det(A)
# end=time.time()
# print("scipy:",end-start)


start=time.time()
np.linalg.det(A)
end=time.time()
print("numpylinalg:",end-start)
