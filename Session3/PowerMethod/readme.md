### Power method to calculate dominant eigen vecotor and eigen value and its comparison with other methods : 


 ## Numpy and c++ Eigen library ; determinant calculation 

 * Matrix elements are chosen randomly 

| Matrix size   | Numpy.linalg library | Eigen library|
| ------------- |:------------------:|:-------------: |
| 2*2           | 0.0003781 sec      | 0.0000729 sec  | 
| 10*10         | 0.0004231 sec      | 0.0000815 sec  | 
| 50*50         | 0.0892782 sec      | 0.0002520 sec  | 
| 100*100       | 0.0482661 sec      | 0.0008089 sec  | 
| 1000*1000     | overflow           | 0.1090905 sec  | 


![Numpy and Eigen](Session3/PowerMethod/determinant.png)



## Comparison between Power method and linalg eigen method 
 * Tolerance limit for error is 0.001
 * Error here is the difference between  Ax and x , x  is the eigen vector in the current iteration 

| Matrix size   | Powermethod        |  error       | Num of interations  | linalg eigen  |
| ------------- |:------------------:|:------------:| :------------------:|:--------------:|
| 2*2           | 0.0001246 sec      | 0.00017393   | 3                   | 0.0267264 sec  |
| 10*10         | 0.0002422 sec      | 0.00045714   | 4                   | 0.0232567 sec  |
| 50*50         | 0.0005238 sec      | 0.00010118   | 4                   | 0.1253390 sec  |
| 100*100       | 0.0007390 sec      | 0.00051229   | 3                   | 0.0805125 sec  |
| 1000*1000     | 0.0202648 sec      | 0.00005619   | 3                   | 2.3136627 sec  |


![powermethod and numpy-lingalg.eigen](Session3/PowerMethod/eigen.png)


## And a comparison between Power Method and simple method and linalg.eigen method for 2*2 Matrix

* for simple method we must solve this equation : | A − λI | = 0 


| Methods          | 1             | 2              | 3              | 4              |
| -------------    |:-------------:|:-------------: |:--------------:|:--------------:|
| **SimpleMethod** | 0.0203061 sec | 0.0199294 sec  | 0.0219039 sec  | 0.0280046 sec  |
| **linalg.eigen** | 0.0003321 sec | 0.0003893 sec  | 0.0007486 sec  | 0.0007152 sec  |
| **Power Method** | 0.0008511 sec | 0.0001170 sec  | 0.0002171 sec  | 0.0002322 sec  |

![Three Methods on 2*2 Matrix](Session3/PowerMethod/3methods.png)







