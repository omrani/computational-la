<%
cfg['compiler_args'] = ['-std=c++11']
cfg['include_dirs'] = ['./eigen']
cfg['compiler_args'] = ['-std=c++11', '-fopenmp']
cfg['linker_args'] = ['-lgomp']
setup_pybind11(cfg)
%>
#include <omp.h>
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include<iostream>
#include <Eigen/Dense>



namespace py = pybind11;
using namespace Eigen;
using namespace std;



void power(MatrixXd A,VectorXd v1) {

      double eigenvalue = 0;
      double error = 1;
      VectorXd v2(v1.size());
      v1=v1/v1.norm();

      int i;
       // #pragma omp parallel
       // #pragma omp parallel for lastprivate(i)
      for(i=0;0.001<error;i++)
       {
         v2=A*v1;
         v2=v2/v2.norm();
         eigenvalue=(v2).transpose()*A*v2;
         error = (A*v2 - eigenvalue*v2).norm();
         v1=v2;
       }

       cout<<"Here is our eigenvalue: "<<eigenvalue<<"\n";
       cout<<"And here is our eigenvector:\n"<<v2<<"\nafter"<<i<<" iterations \n"<<error;

}


PYBIND11_MODULE(ex, m) {
    m.doc() = "auto-compiled c++ extension";
    m.def("power", &power);
}
