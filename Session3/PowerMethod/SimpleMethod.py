import cppimport
import time
import numpy as np
import numpy.linalg as la
import ex9
import ex


# A = np.random.rand(2,2)

def find_eigens(A):

    det=ex9.det(A)
    trace= np.trace(A)
    coeff = [1, -trace, det]
    landas=np.roots(coeff)
    newA=np.subtract(A,np.dot(landas[0],np.identity(2)))
    newA=np.dot(newA,[[1,0],[0,-1]])

    newA2=np.subtract(A,np.dot(landas[1],np.identity(2)))
    newA2=np.dot(newA2,[[1,0],[0,-1]])


    alpha1 = newA[[0],:]/newA[[1],:]
    eigenvectors=[]
    eigenvectors.append(newA[[0],:])
    if(alpha1[0][0]!=alpha1[0][1]) :
        eigenvectors.append(newA[[1],:])
    else:

        alpha2 = newA2[[0],:]/newA[[0],:]
        if(alpha2[0][0]!=alpha2[0][1]) :
            eigenvectors.append(newA2[[0],:])
        else:
            alpha3 = newA2[[1],:]/newA[[0],:]
            if(alpha3[0][0]!=alpha3[0][1]) :
                eigenvectors.append(newA2[[1],:])

    # print(eigenvectors[0],eigenvectors[1])


def find_eigens2(A):
    det=ex9.det(A)
    trace= np.trace(A)
    coeff = [1, -trace, det]
    landas=np.roots(coeff)





#
# start=time.time()
# find_eigens2(A)
# end=time.time()
# print("value :",end-start)


n=int(input())
A = np.random.rand(n,n)

start=time.time()
find_eigens(A)
end=time.time()
print("khodam :",end-start)



start=time.time()
la.eig(A)
end=time.time()
print("linalg:",end-start)


# #
code = cppimport.imp("ex")

# A = np.random.rand(n,n)
v=np.ones((n,1))
start=time.time()
code.power(A,v)
end=time.time()
print("power method  :",end-start)
