import cppimport
import time
import numpy as np
import ex

# code = cppimport.imp("ex")
# import ex
# import sympy
# import scipy.linalg as la

# code = cppimport.imp("ex")

n=int(input())
A = np.random.rand(n,n)
v=np.ones((n,1))
# print(v)

start=time.time()
ex.det(A,v)
end=(time.time())

print(end-start)
# v2=np.array([[2],[1]])

# print("norm",np.linalg.norm((v2-v)))
# print("abs",np.absolute(v2-v))
