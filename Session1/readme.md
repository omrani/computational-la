### Here is a table showing approximate  duration of running sum functions in c++ and python :

###### For different  input size of arrays ranging between 10 to 1M



| Input Size    | Cpp sum       | Python simple sum | Python default list sum  | Numpy array sum |
| ------------- |:-------------:|:-----------------:|:------------------------:|----------------:|
| 10            | 0.0000004 sec | 0.0000259 sec     | 0.0000035 sec            | 0.0000093 sec   |
| 1k            | 0.0000169 sec | 0.0010166 sec     | 0.0000264 sec            | 0.0000808 sec   |
| 100k          | 0.0005640 sec | 0.0793941 sec     | 0.0056893 sec            | 0.0001919 sec   |
| 1M            | 0.0038507 sec | 0.3875801 sec     | 0.0635404 sec            | 0.0012376 sec   |



## Rseults plot

* python simple sum --> maually add items of the list

* python default list sum --> using the built-in sum function on a python list  

* Numpy array sum --> create a numpy array and use numpy sum function 

![compare](Session1/result-s1.png)
