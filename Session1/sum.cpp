#include <iostream>
#include <chrono>

using namespace std;
void sum(int* a, int n);

int main()
{
  int n;
  cin>>n;
  int* array = new int[n];
  for(int i = 0; i < n; i++){
      array[i] = (rand() % n) + 1;
  }

auto start = chrono::high_resolution_clock::now();
sum(array,n);
auto end = chrono::high_resolution_clock::now();
delete[] array;


double time_taken =  chrono::duration_cast<chrono::nanoseconds>(end - start).count();
time_taken *= 1e-9;

cout << "Time taken by program is : " << time_taken<<"sec"<<endl;

return 0;
}

void sum(int* array ,int n)
{
  long int sum=0;
  for(int i=0; i<n;i++)
  {
    sum+= array[i];
  }
  return;
}




///// approximate time taken by program in case of input size 100000 is : 0.00066sec
