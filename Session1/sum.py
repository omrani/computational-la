import random
import numpy as np
import time


list=[]
arr= np.array([])
n= int(input())
for i in range(n):
    num=random.randint(1,n)
    np.append(arr,num)


for i in range(n):
    num=random.randint(1,n)
    list.append(num)



# approximate time taken by program for 100k inputs is
# 0.010089 sec for simple sum 
# 0.000818 sec for python default list sum
# 0.000056 sec for numpy sum

# simple sum
def sum0():
    sum=0
    for i in range(n):
        sum+=list[i]

# python default list sum function
def sum1():
    sum(list)

#numpy sum function
def sum2():
    np.sum(arr[:])



start=time.time()
sum0()
duration0=time.time()-start

start=time.time()
sum1()
duration1=time.time()-start

start=time.time()
sum2()
duration2=time.time()-start


print("Time taken by program is\n%f sec for simple sum \n%f sec for python default list sum \n%f sec for numpy sum "%(duration0,duration1,duration2))
