# This code will generate the density histogram of arbitrary mantissa/exponents



from queue import Queue
from tqdm import tqdm
import matplotlib.pylab as plt
import torch
import numpy as np
import seaborn as sns
from torch.utils.tensorboard import SummaryWriter
from glob import glob

# Function to convert Binary
# of Mantissa to float value.
def convertToInt(mantissa_str, is_normalized=True):

    power_count = -1
    mantissa_int = 0
    for i in mantissa_str:
        # Adding converted value of
        # Binary bits in every
        # iteration to float mantissa.
        mantissa_int += (int(i) * pow(2, power_count))

        # count will decrease by 1
        # as we move toward right.
        power_count -= 1

    # returning mantissa in 1.M form.
    if is_normalized:
        return (mantissa_int + 1)
    return mantissa_int


# Generate all possible binary numbers
#  of n bits in linear time.
def generatePosBinary(n):
    binary_len = n
    n = 2**n - 1
    q = Queue()
    binary_list = []
    binary_list.append('0'*binary_len)
    q.put("1")

    while (n>0):
        n-=1
        s1 = q.get()
        binary_list.append(s1.zfill(binary_len))
        if binary_list[-1][0] == '1':
            # print('First negative number is: ', binary_list[-1])
            binary_list.pop()
            break

        # print('%.180f' % bin_to_ieee32(binary_list[-1]))


        s2 = s1
        q.put(s1 + "0")
        q.put(s2 + "1")

    return binary_list


def bin_to_ieee(ieee_str, exponent, mantissa):

    sign_bit = int(ieee_str[0])
    exponent_bias = int(ieee_str[1: exponent+1], 2)
    exp_bit_num = len(ieee_str[1: exponent+1])
    mantissa_str = ieee_str[exponent + 1:]

    assert 1 + len(ieee_str[1: exponent+1]) + len(ieee_str[exponent + 1:]) == len(ieee_str), 'Wrong number of e,m'

    assert sign_bit == 1 or sign_bit == 0, 'sign bit should be 0/1'

    if exponent_bias != 0:
        exponent_unbias = exponent_bias - (2**(exponent-1) -1)
        mantissa_int = convertToInt(mantissa_str)
        return pow(-1, sign_bit) * mantissa_int * pow(2, exponent_unbias)

    else:
        exp_ = -(2**(exp_bit_num-1) - 2)
        mantissa_int = convertToInt(mantissa_str, is_normalized=False)
        return pow(-1, sign_bit) * mantissa_int * pow(2, exp_)


# Generate all values != Inf
def dec_ieee(exponent_num, mantissa_num, mode='all'):
    number_list = generatePosBinary(exponent_num+mantissa_num+1)
    valid_num_list = []
    for num in number_list:
        if mode == 'all':
            if '0' in num[1: exponent_num+1]:
                decimal_num = bin_to_ieee(num, exponent_num, mantissa_num)
                valid_num_list.append(decimal_num)
                valid_num_list.append(-decimal_num)
        elif mode == 'normal':
            if '0' in num[1: exponent_num + 1] and '1' in num[1: exponent_num + 1]:
                decimal_num = bin_to_ieee(num, exponent_num, mantissa_num)
                valid_num_list.append(decimal_num)
                valid_num_list.append(-decimal_num)
        else:
            raise ValueError("mode should be all or normal!")

    return valid_num_list

def categorize(bins):
    vals = np.zeros([4 + 2000], dtype=np.int64)
    try:
        for i in tqdm(range(1 << 32)):
            f = float(i)
            if f != i:
                vals[0] += 1
            elif f == 0:
                vals[1] += 1
            elif f <= -10:
                vals[2] += 1
            elif f >= 10:
                vals[3] += 1
            #####################################
            else:
                vals[int((f + 10) * 100) + 4] += 1
    except KeyboardInterrupt:
        print('early exit')

def hist_plot(data, title, xrange=1.5, binwidth=0.01):
    plt.hist(data, bins=np.arange(-xrange, xrange + binwidth, binwidth))
    plt.title(title)
    plt.savefig(title.replace('', '_')+'.png')
    plt.cla()

if __name__ == "__main__":
    data_normal = dec_ieee(3, 2, mode='all')
    print(data_normal, len(data_normal))

    data_normal = dec_ieee(3, 2, mode='normal')
    print(data_normal, len(data_normal))
    # for exp in tqdm(range(1, 11)):
    #     for sig in range(1, 11):
    #         data_all = dec_ieee(exp, sig, mode='all')
    #         data_normal = dec_ieee(exp, sig, mode='normal')
    #         np.savetxt('low_precision_numbers/{}_{}_all.txt'.format(exp, sig), data_all)
    #         np.savetxt('low_precision_numbers/{}_{}_normal.txt'.format(exp, sig), data_normal)
